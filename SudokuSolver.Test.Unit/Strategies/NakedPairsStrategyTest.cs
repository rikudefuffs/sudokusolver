﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SudokuSolver.Strategies;
using SudokuSolver.Workers;

namespace SudokuSolver.Test.Unit.Strategies
{
    [TestClass]
    public class NakedPairsStrategyTest
    {
        readonly ISudokuStrategy nakedPairsStrategy = new NakedPairsStrategy(new SudokuMapper());
        [TestMethod]
        public void ShouldEliminateNumbersInRowBasedOnNakedPair()
        {
            int[,] sudokuBoard =
            {
                {1,2,34,5,34,6,7,348,9},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
            };

            int[,] solvedSudokuBoard = nakedPairsStrategy.Solve(sudokuBoard);
            Assert.IsTrue(solvedSudokuBoard[0, 7] == 8);
        }

        [TestMethod]
        public void ShouldEliminateNumbersInColBasedOnNakedPair()
        {
            int[,] sudokuBoard =
            {
                {1,0,0,0,0,0,0,0,0},
                {24,0,0,0,0,0,0,0,0},
                {3,0,0,0,0,0,0,0,0},
                {5,0,0,0,0,0,0,0,0},
                {6,0,0,0,0,0,0,0,0},
                {24,0,0,0,0,0,0,0,0},
                {7,0,0,0,0,0,0,0,0},
                {8,0,0,0,0,0,0,0,0},
                {249,0,0,0,0,0,0,0,0},
            };

            int[,] solvedSudokuBoard = nakedPairsStrategy.Solve(sudokuBoard);
            Assert.IsTrue(solvedSudokuBoard[8, 0] == 9);
        }

        [TestMethod]
        public void ShouldEliminateNumbersInBlockOneBasedOnNakedPair()
        {
            int[,] sudokuBoard =
            {
                {1,2,3,0,0,0,0,0,0},
                {45,6,7,0,0,0,0,0,0},
                {8,45,459,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
            };

            int[,] solvedSudokuBoard = nakedPairsStrategy.Solve(sudokuBoard);
            Assert.IsTrue(solvedSudokuBoard[2, 2] == 9);
        }

        [TestMethod]
        public void ShouldEliminateNumbersInBlockFiveBasedOnNakedPair()
        {
            int[,] sudokuBoard =
            {
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,1,2,3,0,0,0},
                {0,0,0,45,459,7,0,0,0},
                {0,0,0,8,45,6,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
            };

            int[,] solvedSudokuBoard = nakedPairsStrategy.Solve(sudokuBoard);
            Assert.IsTrue(solvedSudokuBoard[4, 4] == 9);
        }

        [TestMethod]
        public void ShouldEliminateNumbersInBlockNineBasedOnNakedPair()
        {
            int[,] sudokuBoard =
            {
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,45,459,7},
                {0,0,0,0,0,0,8,45,6},
                {0,0,0,0,0,0,1,2,3},
            };

            int[,] solvedSudokuBoard = nakedPairsStrategy.Solve(sudokuBoard);
            Assert.IsTrue(solvedSudokuBoard[6, 7] == 9);
        }
    }
}
