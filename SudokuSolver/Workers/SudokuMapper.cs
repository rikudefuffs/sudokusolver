﻿using SudokuSolver.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver.Workers
{
    
    class SudokuMapper
    {
        /// <summary>
        /// Finds the square in which you're operating
        /// </summary>
        public SudokuMap Find(int givenRow, int givenCol)
        {
            /*
             * 0 1 2 | 3 4 5 | 6 7 8
             * 1
             * 2
             * ------|-------|------
             * 3
             * 4
             * 5
             * ------|-------|------
             * 6
             * 7
             * 8
             */
            SudokuMap sudokuMap = new SudokuMap();
            sudokuMap.startRow = givenRow - (givenRow % 3);
            sudokuMap.startColumn = givenCol - (givenCol % 3);
            return sudokuMap;
        }
    }
}
