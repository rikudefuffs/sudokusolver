﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver.Workers
{
    class SudokuBoardStateManager
    {
        public string GenerateState(int[,] sudokuBoard)
        {
            StringBuilder builder = new StringBuilder();
            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                for (int column = 0; column < sudokuBoard.GetLength(1); column++)
                {
                    builder.Append(sudokuBoard[row, column]);
                }
            }
            return builder.ToString();
        }

        public bool IsSolved(int[,] sudokuBoard)
        {
            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                for (int column = 0; column < sudokuBoard.GetLength(1); column++)
                {
                    if ((sudokuBoard[row, column] == 0)
                    || (sudokuBoard[row, column].ToString().Length > 1)) //range of possibilities in a single cell
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}