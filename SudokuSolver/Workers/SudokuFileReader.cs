﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace SudokuSolver.Workers
{
    class SudokuFileReader
    {
        public int[,] ReadFile(string fileName)
        {
            int[,] sudokuBoard = new int[9, 9];
            try
            {
                string[] sudokuBoardLines = File.ReadAllLines(fileName);
                int row = 0;
                foreach (string line in sudokuBoardLines)
                {
                    /*Structure of a sudoku line:
                     |9| | |2|3|7|6|8| |
                     */
                    string[] sudokuBoardLineElements = line.Split('|').Skip(1).Take(9).ToArray(); //skip the first delimiter and take the next 9 ones
                    int column = 0;
                    foreach (string element in sudokuBoardLineElements)
                    {
                        if (element == " ")
                        {
                            sudokuBoard[row, column] = 0;
                        }
                        else
                        {
                            sudokuBoard[row, column] = Convert.ToInt32(element);
                        }
                        column++;
                    }
                    row++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Something went from while reading the file " + ex.Message);
            }
            return sudokuBoard;
        }
    }
}
