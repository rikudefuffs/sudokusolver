﻿using SudokuSolver.Strategies;
using SudokuSolver.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SudokuMapper sudokuMapper = new SudokuMapper();
                SudokuBoardStateManager sudokuBoardStateManager = new SudokuBoardStateManager();
                SudokuSolverEngine sudokuSolverEngine = new SudokuSolverEngine(sudokuBoardStateManager, sudokuMapper);
                SudokuFileReader sudokuFileReader = new SudokuFileReader();
                SudokuBoardDisplayer sudokuBoardDisplayer = new SudokuBoardDisplayer();
                Console.WriteLine("Insert the Sudoku file name: ");
                string fileName = Console.ReadLine();
                int[,] sudokuBoard = sudokuFileReader.ReadFile(fileName);
                sudokuBoardDisplayer.Display("Initial state: ", sudokuBoard);
                bool isSudokuSolved = sudokuSolverEngine.Solve(sudokuBoard, true);
                sudokuBoardDisplayer.Display("Final state: ", sudokuBoard);
                if (isSudokuSolved)
                {
                    Console.WriteLine("Solved!");
                }
                else
                {
                    Console.WriteLine("You current algorithm(s) were not enough  to solve this sudoku puzzle...");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Sudoku puzzle can't be solved because there was an error: {0}", ex.Message + ex.StackTrace);
            }
            Console.Read();
        }
    }
}
