﻿using SudokuSolver.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver.Strategies
{
    class SudokuSolverEngine
    {
        readonly SudokuBoardStateManager sudokuBoardStateManager;
        readonly SudokuMapper sudokuMapper;

        public SudokuSolverEngine(SudokuBoardStateManager manager, SudokuMapper mapper)
        {
            sudokuBoardStateManager = manager;
            sudokuMapper = mapper;
        }

        public bool Solve(int[,] sudokuBoard, bool printIntermediateStates)
        {
            List<ISudokuStrategy> strategies = new List<ISudokuStrategy>()
            {
                new SimpleMarkupStrategy(sudokuMapper),
                new NakedPairsStrategy(sudokuMapper)
            };

            string currentState = sudokuBoardStateManager.GenerateState(sudokuBoard);
            string nextState = sudokuBoardStateManager.GenerateState(strategies.First().Solve(sudokuBoard));

            while (!sudokuBoardStateManager.IsSolved(sudokuBoard) && (currentState != nextState))
            {
                currentState = nextState;
                foreach (var strategy in strategies)
                {
                    nextState = sudokuBoardStateManager.GenerateState(strategy.Solve(sudokuBoard));
                    if (printIntermediateStates)
                    {
                        SudokuBoardDisplayer.DisplayStatic("State after '" + strategy.ToString() + "' strategy: ", sudokuBoard);
                    }
                }
            }

            return sudokuBoardStateManager.IsSolved(sudokuBoard);
        }
    }
}
