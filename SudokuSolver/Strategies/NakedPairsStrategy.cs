﻿using SudokuSolver.Data;
using SudokuSolver.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver.Strategies
{
    class NakedPairsStrategy : ISudokuStrategy
    {
        readonly SudokuMapper sudokuMapper;

        public NakedPairsStrategy(SudokuMapper mapper)
        {
            sudokuMapper = mapper;
        }

        public int[,] Solve(int[,] sudokuBoard)
        {
            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                for (int column = 0; column < sudokuBoard.GetLength(1); column++)
                {
                    EliminateNakedPairFromOthersInRow(sudokuBoard, row, column);
                    EliminateNakedPairFromOthersInColumn(sudokuBoard, row, column);
                    EliminateNakedPairFromOthersInBlock(sudokuBoard, row, column);
                }
            }
            return sudokuBoard;
        }

        void EliminateNakedPairFromOthersInRow(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            if (!HasNakedPairInRow(sudokuBoard, givenRow, givenColumn)) { return; }
            for (int column = 0; column < sudokuBoard.GetLength(1); column++)
            {
                if ((sudokuBoard[givenRow, column] != sudokuBoard[givenRow, givenColumn])
                && (sudokuBoard[givenRow, column].ToString().Length > 1))
                {
                    EliminateNakedPair(sudokuBoard, sudokuBoard[givenRow, givenColumn], givenRow, column);
                }
            }
        }

        void EliminateNakedPairFromOthersInColumn(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            if (!HasNakedPairInColumn(sudokuBoard, givenRow, givenColumn)) { return; }
            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                if ((sudokuBoard[row, givenColumn] != sudokuBoard[givenRow, givenColumn])
                && (sudokuBoard[row, givenColumn].ToString().Length > 1))
                {
                    EliminateNakedPair(sudokuBoard, sudokuBoard[givenRow, givenColumn], row, givenColumn);
                }
            }
        }

        void EliminateNakedPairFromOthersInBlock(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            SudokuMap sudokuMap = sudokuMapper.Find(givenRow, givenColumn);
            if (!HasNakedPairInBlock(sudokuMap, sudokuBoard, givenRow, givenColumn)) { return; }

            for (int row = sudokuMap.startRow; row < (sudokuMap.startRow + 3); row++)
            {
                for (int column = sudokuMap.startColumn; column < (sudokuMap.startColumn + 3); column++)
                {
                    if ((sudokuBoard[row, column].ToString().Length > 1) && (sudokuBoard[row, column] != sudokuBoard[givenRow, givenColumn]))
                    {
                        EliminateNakedPair(sudokuBoard, sudokuBoard[givenRow, givenColumn], row, column);
                    }
                }
            }
        }

        bool HasNakedPairInBlock(SudokuMap sudokuMap, int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            for (int row = sudokuMap.startRow; row < (sudokuMap.startRow + 3); row++)
            {
                for (int column = sudokuMap.startColumn; column < (sudokuMap.startColumn + 3); column++)
                {
                    if ((row == givenRow) && (column == givenColumn)) { continue; }
                    if (IsNakedPair(sudokuBoard[row, column], sudokuBoard[givenRow, givenColumn]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        bool IsNakedPair(int firstPair, int secondPair)
        {
            return (firstPair.ToString().Length == 2) && (firstPair == secondPair);
        }

        /// <summary>
        /// I.E: pair is 12, cell is 123. --> cell remains with 3
        /// </summary>
        /// <param name="sudokuBoard"></param>
        /// <param name="valuesToEliminate"></param>
        /// <param name="rowWhereActing"></param>
        /// <param name="columnWhereActing"></param>
        void EliminateNakedPair(int[,] sudokuBoard, int valuesToEliminate, int rowWhereActing, int columnWhereActing)
        {
            char[] valuesToEliminateArray = valuesToEliminate.ToString().ToCharArray();
            foreach (var valueToEliminate in valuesToEliminateArray)
            {
                //I.E: pair is 12, cell is 123. --> cell remains with 3
                string remainingDigits = sudokuBoard[rowWhereActing, columnWhereActing].ToString().Replace(valueToEliminate.ToString(), string.Empty);
                sudokuBoard[rowWhereActing, columnWhereActing] = Convert.ToInt32(remainingDigits);
            }
        }

        bool HasNakedPairInRow(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            for (int column = 0; column < sudokuBoard.GetLength(1); column++)
            {
                if ((givenColumn != column)
                && IsNakedPair(sudokuBoard[givenRow, column], sudokuBoard[givenRow, givenColumn]))
                {
                    return true;
                }
            }
            return false;
        }

        bool HasNakedPairInColumn(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                if ((givenRow != row)
                && IsNakedPair(sudokuBoard[row, givenColumn], sudokuBoard[givenRow, givenColumn]))
                {
                    return true;
                }
            }
            return false;
        }
    }
}