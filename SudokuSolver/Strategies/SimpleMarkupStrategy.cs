﻿using SudokuSolver.Data;
using SudokuSolver.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver.Strategies
{
    /// <summary>
    /// A strategy that understands what are the digits that can be put in a single cell, considering its column, row and group
    /// </summary>
    class SimpleMarkupStrategy : ISudokuStrategy
    {
        readonly SudokuMapper sudokuMapper;

        public SimpleMarkupStrategy(SudokuMapper mapper)
        {
            sudokuMapper = mapper;
        }

        public int[,] Solve(int[,] sudokuBoard)
        {

            for (int row = 0; row < sudokuBoard.GetLength(0); row++)
            {
                for (int column = 0; column < sudokuBoard.GetLength(1); column++)
                {
                    if ((sudokuBoard[row, column] == 0)
                    || (sudokuBoard[row, column].ToString().Length > 1))
                    {
                        //find the possibilities in the block, column and row, then intersect them
                        int possibilitisInRowAndColumn = GetPossibilitiesInRowAndColumn(sudokuBoard, row, column);
                        int possibilitisInBlock = GetPossibilitiesInBlock(sudokuBoard, row, column);
                        sudokuBoard[row, column] = GetPossibilityIntersection(possibilitisInRowAndColumn, possibilitisInBlock);
                    }
                }
            }

            return sudokuBoard;
        }


        int GetPossibilitiesInRowAndColumn(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            int[] possibilities = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            for (int column = 0; column < 9; column++)
            {
                if (IsValidSingle(sudokuBoard[givenRow, column])) //then you have already put that number there and you can't reuse it
                {
                    possibilities[sudokuBoard[givenRow, column] - 1] = 0; //mark it as unusable
                }
            }

            for (int row = 0; row < 9; row++)
            {
                if (IsValidSingle(sudokuBoard[row, givenColumn]))
                {
                    possibilities[sudokuBoard[row, givenColumn] - 1] = 0;
                }
            }
            return Convert.ToInt32(String.Join(string.Empty, possibilities.Select(p => p).Where(p => p != 0)));
        }
        int GetPossibilitiesInBlock(int[,] sudokuBoard, int givenRow, int givenColumn)
        {
            int[] possibilities = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            SudokuMap map = sudokuMapper.Find(givenRow, givenColumn);

            for (int row = map.startRow; row < map.startRow + 3; row++)
            {
                for (int column = map.startColumn; column < map.startColumn + 3; column++)
                {
                    if (IsValidSingle(sudokuBoard[row, column]))
                    {
                        possibilities[sudokuBoard[row, column] - 1] = 0;
                    }
                }
            }

            return Convert.ToInt32(String.Join(string.Empty, possibilities.Select(p => p).Where(p => p != 0)));
        }
        int GetPossibilityIntersection(int possibilitisInRowAndColumn, int possibilitisInBlock)
        {
            char[] possibilitiesInRowAndColAsArray = possibilitisInRowAndColumn.ToString().ToCharArray();
            char[] possibilitiesInBlockAsArray = possibilitisInBlock.ToString().ToCharArray();
            var possibilitiesSubset = possibilitiesInRowAndColAsArray.Intersect(possibilitiesInBlockAsArray);
            return Convert.ToInt32(string.Join("", possibilitiesSubset));
        }

        /// <summary>
        /// Checks if a digit is a single digit
        /// </summary>
        /// <param name="cellDigit"></param>
        /// <returns></returns>
        bool IsValidSingle(int cellDigit)
        {
            return (cellDigit != 0) && (cellDigit.ToString().Length == 1);
        }
    }
}
